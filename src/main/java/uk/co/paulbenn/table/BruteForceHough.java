package uk.co.paulbenn.table;

import lombok.Builder;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.BiPredicate;

@Builder
public class BruteForceHough {
    // dp - resolution
    // too small = perfect circles only, too large = noise counts as circle
    private final ParameterDefinition resolution;

    private final ParameterDefinition distanceBetweenCentres;

    // param1 - how strong the edges of the circles need to be
    // too low = too many circles, too high = no circles
    private final ParameterDefinition edgeDetectionThreshold;

    // param2 - how many edge points it needs to find to declare that it's found a circle
    // too low = too many circles, too high = no circles
    private final ParameterDefinition minVotes;

    private final ParameterDefinition radius;

    public Mat findAtMost(int maxCircles, Mat src) {
        return houghCircles(maxCircles, (current, max) -> current <= maxCircles, src);
    }

    public Mat findAtLeast(int minCircles, Mat src) {
        return houghCircles(minCircles, (current, min) -> current >= minCircles, src);
    }

    private Mat houghCircles(int limit, BiPredicate<Integer, Integer> condition, Mat src) {
        List<ParameterDefinition> parameterDefinitions = inOrder();

        ParameterDefinition first = parameterDefinitions.get(0);
        ParameterDefinition second = parameterDefinitions.get(1);
        ParameterDefinition third = parameterDefinitions.get(2);
        ParameterDefinition fourth = parameterDefinitions.get(3);
        ParameterDefinition fifth = parameterDefinitions.get(4);

        for (double p1 = first.initialValue(); first.hasNext(p1); p1 = first.getNextValue(p1)) {
            for (double p2 = second.initialValue(); second.hasNext(p2); p2 = second.getNextValue(p2)) {
                for (double p3 = third.initialValue(); third.hasNext(p3); p3 = third.getNextValue(p3)) {
                    for (double p4 = fourth.initialValue(); fourth.hasNext(p4); p4 = fourth.getNextValue(p4)) {
                        for (double p5 = fifth.initialValue(); fifth.hasNext(p5); p5 = fifth.getNextValue(p5)) {
                            HoughParameters parameters = fromOrderedParameters(
                                parameterDefinitions,
                                new double[] { p1, p2, p3, p4, p5 }
                            );
                            Mat circles = new Mat();
                            Imgproc.HoughCircles(
                                src,
                                circles,
                                Imgproc.HOUGH_GRADIENT,
                                parameters.getResolution(),
                                parameters.getDistanceBetweenCentres(),
                                parameters.getEdgeDetectionThreshold(),
                                parameters.getMinVotes(),
                                parameters.getRadius() - 1,
                                parameters.getRadius() + 1
                            );
                            if (condition.test(circles.cols(), limit)) {
                                return circles;
                            }
                        }
                    }
                }
            }
        }

        return new Mat();
    }

    private HoughParameters fromOrderedParameters(List<ParameterDefinition> orderedParameterDefinitions, double[] orderedValues) {
        HoughParameters parameters = new HoughParameters();

        for (int i = 0; i < orderedParameterDefinitions.size(); i++) {
            orderedParameterDefinitions.get(i).getName().setParameter(orderedValues[i], parameters);
        }

        return parameters;
    }

    private List<ParameterDefinition> inOrder() {
        List<ParameterDefinition> parameterDefinitions = new ArrayList<>();

        resolution.setName(ParameterName.RESOLUTION);
        distanceBetweenCentres.setName(ParameterName.DISTANCE_BETWEEN_CENTRES);
        edgeDetectionThreshold.setName(ParameterName.EDGE_DETECTION_THRESHOLD);
        minVotes.setName(ParameterName.MIN_VOTES);
        radius.setName(ParameterName.RADIUS);

        parameterDefinitions.add(resolution);
        parameterDefinitions.add(distanceBetweenCentres);
        parameterDefinitions.add(edgeDetectionThreshold);
        parameterDefinitions.add(minVotes);
        parameterDefinitions.add(radius);

        parameterDefinitions.sort(Comparator.comparingInt(ParameterDefinition::getOrder));

        return parameterDefinitions;
    }
}
