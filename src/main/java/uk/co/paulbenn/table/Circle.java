package uk.co.paulbenn.table;

import lombok.Data;

@Data
public class Circle implements Comparable<Circle> {
    private final double x;
    private final double y;
    private final double radius;

    @Override
    public int compareTo(Circle o) {
        int cmp = Double.compare(this.x, o.x);
        if (cmp == 0) {
            cmp = Double.compare(this.y, o.y);
        }
        return cmp;
    }
}
