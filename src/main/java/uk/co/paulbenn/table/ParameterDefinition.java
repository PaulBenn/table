package uk.co.paulbenn.table;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

public class ParameterDefinition {

    @Getter
    @Setter
    private ParameterName name;

    private final double min;

    private final double max;

    private final double step;

    @Getter
    private final int order;

    private final boolean startAtMax;

    @Builder
    public ParameterDefinition(double min, double max, double step, int order, boolean startAtMax) {
        this.min = min;
        this.max = max;
        this.step = step;
        this.order = order;
        this.startAtMax = startAtMax;
    }

    public double initialValue() {
        return startAtMax ? max : min;
    }

    public boolean hasNext(double current) {
        return startAtMax ? (current >= min) : (current <= max);
    }

    public double getNextValue(double current) {
        return startAtMax ? (current - step) : (current + step);
    }
}
