package uk.co.paulbenn.table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HoughParameters {

    // dp - resolution
    // too small = perfect circles only, too large = noise counts as circle
    private double resolution;

    // distance between circle centres
    private double distanceBetweenCentres;

    // param1 - how strong the edges of the circles need to be
    // too low = too many circles, too high = no circles
    private double edgeDetectionThreshold;

    // param2 - how many edge points it needs to find to declare that it's found a circle
    // too low = too many circles, too high = no circles
    private double minVotes;

    // radius of circles
    private int radius;
}
