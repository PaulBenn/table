package uk.co.paulbenn.table;

import org.opencv.core.Core;

public final class OpenCv {

    private static boolean loaded = false;

    public static void loadNativeCode() {
        if (!loaded) {
            System.loadLibrary("lib/" + Core.NATIVE_LIBRARY_NAME);
            loaded = true;
        }
    }
}
