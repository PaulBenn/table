package uk.co.paulbenn.table;

import nom.tam.fits.Fits;
import nom.tam.fits.FitsException;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FitsReader {

    private static final String[] ACCEPTED_FITS_EXTENSIONS = { "fit", "fits" };

    public List<LazyLoadedFits> readAll(Path directory) {
        if (!Files.isDirectory(directory)) {
            throw new IllegalArgumentException("must be a directory: " + directory);
        }

       try (Stream<Path> entries = Files.walk(directory)) {
           return entries.filter(Files::isRegularFile)
               .filter(this::isFits)
               .map(Path::toFile)
               .map(this::getAsFits)
               .map(LazyLoadedFits::new)
               .collect(Collectors.toList());
       } catch (IOException e) {
           throw new UncheckedIOException(e);
       }
    }

    private Fits getAsFits(File file) {
        try {
            return new Fits(file);
        } catch (FitsException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean isFits(Path path) {
        return getFileExtension(path.getFileName().toString())
            .map(String::toLowerCase)
            .filter(extension -> Arrays.asList(ACCEPTED_FITS_EXTENSIONS).contains(extension))
            .isPresent();
    }

    public Optional<String> getFileExtension(String filename) {
        return Optional.ofNullable(filename)
            .filter(f -> f.contains("."))
            .map(f -> f.substring(filename.lastIndexOf(".") + 1));
    }
}
