package uk.co.paulbenn.table;

import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.Point;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.List;

public class MatRenderer {

    public void overlayCircles(Mat src, List<Circle> circles) {
        for (Circle circle : circles) {
            Point center = new Point(Math.round(circle.getX()), Math.round(circle.getY()));

            // center point
            Imgproc.circle(src, center, 1, OpenCvColour.BLACK, 3, 8, 0 );
            // outline
            Imgproc.circle(src, center, (int) Math.round(circle.getRadius()), OpenCvColour.WHITE, 2, 8, 0 );
        }
    }

    public void render(Mat image) {
        JFrame frame = new JFrame(image.toString());

        frame.getContentPane().setLayout(new FlowLayout());
        frame.getContentPane().add(new JLabel(new ImageIcon(encodeToPng(image))));

        // fit to contents
        frame.pack();

        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private BufferedImage encodeToPng(Mat image) {
        MatOfByte byteBuffer = new MatOfByte();
        Imgcodecs.imencode(".png", image, byteBuffer);
        try {
            return ImageIO.read(new ByteArrayInputStream(byteBuffer.toArray()));
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
