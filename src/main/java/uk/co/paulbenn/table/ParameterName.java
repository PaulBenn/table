package uk.co.paulbenn.table;

public enum ParameterName {
    RESOLUTION {
        @Override
        public void setParameter(double value, HoughParameters parameters) {
            parameters.setResolution(value);
        }
    },
    DISTANCE_BETWEEN_CENTRES {
        @Override
        public void setParameter(double value, HoughParameters parameters) {
            parameters.setDistanceBetweenCentres(value);
        }
    },
    EDGE_DETECTION_THRESHOLD {
        @Override
        public void setParameter(double value, HoughParameters parameters) {
            parameters.setEdgeDetectionThreshold(value);
        }
    },
    MIN_VOTES {
        @Override
        public void setParameter(double value, HoughParameters parameters) {
            parameters.setMinVotes(value);
        }
    },
    RADIUS {
        @Override
        public void setParameter(double value, HoughParameters parameters) {
            parameters.setRadius((int) value);
        }
    };

    public abstract void setParameter(double value, HoughParameters parameters);
}
