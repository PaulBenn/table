package uk.co.paulbenn.table;

import lombok.Getter;
import nom.tam.fits.BasicHDU;
import nom.tam.fits.Fits;
import nom.tam.fits.FitsException;
import nom.tam.fits.ImageHDU;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

import java.io.IOException;
import java.io.UncheckedIOException;

public class LazyLoadedFits {

    // center on original = (point.x + SIDEBAR_WIDTH), (original.MAX_Y - point.y);
    private static final int SIDEBAR_WIDTH = 25;

    private final Fits fits;

    @Getter
    private int offset;

    @Getter
    private short[][] data;

    public LazyLoadedFits(Fits fits) {
        this.fits = fits;
    }

    public Mat toOpenCvMatrix() {
        load();

        Mat mat = new Mat(
            getRows(),
            getColumns() - (SIDEBAR_WIDTH * 2),
            CvType.CV_32FC1
        );

        short[][] rawData = getData();
        int rows = rawData.length;
        int columns = rawData[0].length;

        int offset = getOffset();

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns - SIDEBAR_WIDTH; j++) {
                mat.put(rows - 1 - i, j, rawData[i][j + SIDEBAR_WIDTH] + offset);
            }
        }

        Core.normalize(mat, mat, 0, 255, Core.NORM_MINMAX);

        mat.convertTo(mat, CvType.CV_8UC1);

        return mat;
    }

    public int getRows() {
        return this.data.length;
    }

    public int getColumns() {
        return this.data[0].length;
    }

    private void load() {
        try {
            BasicHDU<?>[] headerDataUnits = this.fits.read();
            this.data = (short[][]) ((ImageHDU) headerDataUnits[1]).getTiler().getCompleteImage();
            this.offset = (int) headerDataUnits[1].getBZero();
        } catch (FitsException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
