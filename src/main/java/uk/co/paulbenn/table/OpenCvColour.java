package uk.co.paulbenn.table;

import org.opencv.core.Scalar;

public final class OpenCvColour {

    public static final Scalar BLACK = new Scalar(0, 100, 100);

    public static final Scalar GREY = new Scalar(128, 128, 128);

    public static final Scalar WHITE = new Scalar(255, 255, 255);
}
