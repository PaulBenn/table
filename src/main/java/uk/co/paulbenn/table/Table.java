package uk.co.paulbenn.table;

import org.opencv.core.Mat;

import java.nio.file.Paths;
import java.util.List;

// Telescope Alignment through Brute-force LEarning
public class Table {

    public static void main(String[] args) {
        OpenCv.loadNativeCode();

        List<LazyLoadedFits> lazyLoadedFits = new FitsReader().readAll(Paths.get(args[0]));

        ContrastAdjuster contrastAdjuster = new ContrastAdjuster();

        CircleDetector circleDetector = new CircleDetector();

        MatRenderer matRenderer = new MatRenderer();

        for (LazyLoadedFits image : lazyLoadedFits) {
            Runnable circleDetection = () -> {
                Mat imageMat = image.toOpenCvMatrix();

                contrastAdjuster.adjustContrast(imageMat);

                List<Circle> circles = circleDetector.detect(imageMat, 20);

                matRenderer.overlayCircles(imageMat, circles);
                matRenderer.render(imageMat);
            };

            circleDetection.run();
        }
    }
}
