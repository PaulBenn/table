package uk.co.paulbenn.table;

import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.CLAHE;
import org.opencv.imgproc.Imgproc;

public class ContrastAdjuster {

    public void adjustContrast(Mat mat) {
        // approximate feature size = 58 diameter
        CLAHE clahe = Imgproc.createCLAHE(30, new Size(58, 58));
        clahe.apply(mat, mat);
    }
}
