package uk.co.paulbenn.table;

import lombok.AllArgsConstructor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CircleSets {

    public static List<Circle> intersection(List<Circle> a, List<Circle> b, int delta) {
        Collections.sort(a);
        Collections.sort(b);

        List<Circle> similar = new ArrayList<>();
        for (Circle aCircle : a) {
            for (Circle bCircles : b) {
                if (distanceBetweenCentres(aCircle, bCircles) < delta) {
                    similar.add(average(aCircle, bCircles));
                }
            }
        }

        return similar;
    }

    private static double distanceBetweenCentres(Circle a, Circle b) {
        return Math.abs(
            Math.sqrt(
                (b.getX() - a.getX()) * (b.getX() - a.getX()) + (b.getY() - a.getY()) * (b.getY() - a.getY())
            )
        );
    }

    private static Circle average(Circle a, Circle b) {
        return new Circle(
            (a.getX() + b.getX()) / 2,
            (a.getY() + b.getY()) / 2,
            (a.getRadius() + b.getRadius()) / 2
        );
    }
}
