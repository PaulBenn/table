package uk.co.paulbenn.table;

import org.opencv.core.Mat;

import java.util.ArrayList;
import java.util.List;

public class CircleDetector {

    public List<Circle> detect(Mat mat, int minCircles) {
        BruteForceHough highToLowResolution = startAtHighResolution();
        BruteForceHough lowToHighResolution = startAtLowResolution();

        List<Circle> circlesInCommon = null;

        for (int circles = minCircles; circles < 50; circles++) {
            List<Circle> firstPass = houghCirclesToList(highToLowResolution.findAtLeast(circles, mat));
            List<Circle> secondPass = houghCirclesToList(lowToHighResolution.findAtMost(circles, mat));

            circlesInCommon = CircleSets.intersection(firstPass, secondPass, 1);

            if (circlesInCommon.size() >= minCircles) {
                break;
            }
        }

        return circlesInCommon;
    }

    public List<Circle> houghCirclesToList(Mat houghCircles) {
        List<Circle> circles = new ArrayList<>();

        for (int x = 0; x < houghCircles.cols(); x++) {
            double[] circle = houghCircles.get(0, x);
            circles.add(new Circle(circle[0], circle[1], circle[2]));
        }

        return circles;
    }

    private static BruteForceHough startAtHighResolution() {
        return BruteForceHough.builder()
            .resolution(
                ParameterDefinition.builder()
                    .startAtMax(false)
                    .min(1)
                    .max(9)
                    .step(0.5)
                    .order(2)
                    .build()
            )
            .distanceBetweenCentres(
                ParameterDefinition.builder()
                    .min(30)
                    .max(30)
                    .step(1)
                    .order(0)
                    .build()
            )
            .edgeDetectionThreshold(
                ParameterDefinition.builder()
                    .min(127)
                    .max(127)
                    .step(1)
                    .order(0)
                    .build()
            )
            .minVotes(
                ParameterDefinition.builder()
                    .startAtMax(false)
                    .min(30)
                    .max(100)
                    .step(2)
                    .order(1)
                    .build()
            )
            .radius(
                ParameterDefinition.builder()
                    .startAtMax(false)
                    .min(24)
                    .max(28)
                    .step(1)
                    .order(3)
                    .build()
            )
            .build();
    }

    private static BruteForceHough startAtLowResolution() {
        return BruteForceHough.builder()
            .resolution(
                ParameterDefinition.builder()
                    .startAtMax(true)
                    .min(1)
                    .max(9)
                    .step(0.5)
                    .order(2)
                    .build()
            )
            .distanceBetweenCentres(
                ParameterDefinition.builder()
                    .min(30)
                    .max(30)
                    .step(1)
                    .order(0)
                    .build()
            )
            .edgeDetectionThreshold(
                ParameterDefinition.builder()
                    .min(127)
                    .max(127)
                    .step(1)
                    .order(0)
                    .build()
            )
            .minVotes(
                ParameterDefinition.builder()
                    .startAtMax(false)
                    .min(30)
                    .max(100)
                    .step(2)
                    .order(1)
                    .build()
            )
            .radius(
                ParameterDefinition.builder()
                    .startAtMax(false)
                    .min(24)
                    .max(28)
                    .step(1)
                    .order(3)
                    .build()
            )
            .build();
    }
}
